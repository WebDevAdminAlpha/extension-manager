#!/usr/bin/bash
#
# We build an archive that assumes all deps are already installed
# this will be used on windows to ensure an installer is available
#

export PYTHONPATH="."
PYTHON=`which python3`
VERSION=`$PYTHON -m inkman -v`

mkdir -p build
TARGET="build/extensions-manager-fallback-$VERSION.zip"

PKG="org.inkscape.extension.inkman.json"
LST="files.lst"
$PYTHON -m inkman -p $PKG > $LST

zip "$TARGET" -@ < $LST

rm $PKG $LST
